import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import jeudelavie.controleur.JeuDeLaVieController;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {

		JeuDeLaVieController jeu = new JeuDeLaVieController();
		jeu.Init();
		Scene sc = new Scene(jeu.getVue());
		primaryStage.setTitle("Jeu De La Vie");
		primaryStage.setScene(sc);
		primaryStage.show();
	}

	public static void main(String[] args) {

		launch(args);

	}

}
