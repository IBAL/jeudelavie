package jeudelavie.controleur;

import javafx.application.Platform;
import javafx.concurrent.Task;

/**
 * Task utilisé par le service EvaluationService
 */
public class EvaluationTask extends Task<JeuDeLaVieController> {
	private static final int  sleepTime= 10000; 
	private JeuDeLaVieController controller;
	public EvaluationTask( JeuDeLaVieController controller) 
	{
		this.controller=controller;
	}
	@Override
	protected JeuDeLaVieController call() throws Exception {
		this.controller.getModele().getEtat().setValue(true);
		while(true) {
			if(this.isCancelled()) {
				break;
			}
			int sleep=(int) (100+sleepTime-(100*(this.controller.getModele().getSleepTime().getValue())));
			Platform.runLater(()->{
			this.controller.getModele().evoluerMatrice();
			this.controller.getVue().refresh(this.controller.getModele().getMatriceJeu());});
			Thread.sleep(sleep);
		}
		return null;
	}

}
