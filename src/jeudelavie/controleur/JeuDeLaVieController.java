package jeudelavie.controleur;

import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseButton;
import javafx.scene.input.ScrollEvent;
import javafx.scene.transform.Scale;
import jeudelavie.model.Factory;
import jeudelavie.model.ModelJeuDeLaVie;
import jeudelavie.vue.Board;

import jeudelavie.vue.VueJeuDeLaVie;

public class JeuDeLaVieController {
	private ModelJeuDeLaVie modele;
	private VueJeuDeLaVie vue;
	private EvaluationService service;
	
	/**
	 * constructeur par defaut du controleur
	 */
	public JeuDeLaVieController() {
		this.modele = new ModelJeuDeLaVie();

		this.vue = new VueJeuDeLaVie(this.modele.getTaille(), this.modele.getTailleTampon(),
				ModelJeuDeLaVie.getMortSolitudeListe(), ModelJeuDeLaVie.getMortAsphyxieListe(),
				ModelJeuDeLaVie.getVieMinListe(), ModelJeuDeLaVie.getVieMaxListe(),
				jeudelavie.model.Factory.getModelsArray());

	}

	/**
	 * Initialiser : les evenements et les binds, sur les Button, les evenements sur les cellules, les TextField ...
	 * 				   
	 */
	public void Init() {
		
		// création du service
		this.service = new EvaluationService(this);

		// met en action le Button Quit
		this.vue.getQuit().setOnAction(e -> {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Fermeture de l'application");
			alert.setHeaderText("Voulez-vous vraiment fermer l'application ?");
			Optional<ButtonType> option = alert.showAndWait();
			if (option.get() == ButtonType.OK)
				Platform.exit();

		});

		// plateau
		// Ajouter un événemnt de type Scroll sur le plateau du jeu
		this.vue.getPlateau().getJeuBoard().setOnScroll((ScrollEvent event) -> {
			
			double zoomFactor = 1.05;
			double deltaY = event.getDeltaY();
			double X = event.getX();
			double Y = event.getY();
			if (deltaY < 0) {
				zoomFactor = 2.0 - zoomFactor;
			}
			Board b = this.vue.getPlateau().getJeuBoard();
			Scale s= new Scale();
			s.setX(b.getScaleX() * zoomFactor);
			s.setY(b.getScaleY() * zoomFactor);
			s.setPivotX(X);
			s.setPivotY(Y);
			b.getTransforms().add(s);

		});
		
		//Ajouter des événements sur les cellules
		this.addActionsClickPlateau();

		// edition Plateau
		// Ajouter un écouteur sur la propriété text du TextField taille
		// La taille doit être un nombre entier entre 1 et la taille maximale définie au model du jeu de la vie. Si la valeur entré un plus grand que la taille maximale, la valeur est remise automatiquement à le max dans le TextField
		this.vue.getEdition().getTaille().getTextF().textProperty()
				.addListener((ObservableValue, oldValue, newValue) -> {
					if (!newValue.isEmpty()) {
						String Val = newValue.replaceAll("^0+", "");
						if (!Val.matches("[1-9][0-9]*")) {
							Platform.runLater(() -> this.vue.getEdition().getTaille().getTextF()
									.setText(Val.replaceAll("[^\\d]", "")));
						} else {
							if (Integer.parseInt(Val) > ModelJeuDeLaVie.getTailleMatriceMax()) {
								Platform.runLater(() -> this.vue.getEdition().getTaille().getTextF()
										.setText(String.valueOf(ModelJeuDeLaVie.getTailleMatriceMax())));
							} else {
								Platform.runLater(() -> {
									this.vue.getEdition().getTaille().getTextF().setText(Val);
									this.vue.getEdition().getTaille().getTextF().positionCaret(Val.length());
								});

							}
						}
					}
				});
		
		// Ajouter un écouteur sur la propriété text du TextField proba
		// La proba doit être un nombre entier entre 1 et 100. Si la valeur entré un plus grand que 100, la valeur est remise automatiquement à 100 dans le TextField
		this.vue.getEdition().getProba().getTextF().textProperty()
				.addListener((ObservableValue, oldValue, newValue) -> {

					if (!newValue.isEmpty()) {
						String Val = newValue.replaceAll("^0+", "");
						if (!Val.matches("[1-9][0-9]*")) {
							Platform.runLater(() -> this.vue.getEdition().getProba().getTextF()
									.setText(Val.replaceAll("[^\\d]", "")));
						} else {
							if (Integer.parseInt(Val) > 100) {
								Platform.runLater(() -> this.vue.getEdition().getProba().getTextF().setText("100"));
							} else {
								Platform.runLater(() -> {
									this.vue.getEdition().getProba().getTextF().setText(Val);
									this.vue.getEdition().getProba().getTextF().positionCaret(Val.length());
								});
							}
						}
					}
				});

		// met en action le Button submit Taille 
		this.vue.getEdition().getTaille().getSubmit().setOnAction(eh -> {
			if (!this.modele.getEtat().getValue()) {
				if (!this.vue.getEdition().getTaille().getTextF().getText().isBlank()) {
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("modification de  taille de plateau");
					alert.setContentText("Voulez-vous vraiment \n redimensionner la taille de plateau ?");
					Optional<ButtonType> option = alert.showAndWait();
					if (option.get() == ButtonType.OK) {
						int v = Integer.parseInt(this.vue.getEdition().getTaille().getTextF().getText());
						this.modele.modifierTaille(v);
						this.vue.refresh(this.modele.getMatriceJeu());
						this.vue.getEdition().getTaille().getTextF().setText("");
						this.addActionsClickPlateau();
					}
				}
			} else {
				this.AlertShow("avertissement", "il faut arrêter la simulation!");
			}
		});

		// met en action le Button submit Alea
		this.vue.getEdition().getProba().getSubmit().setOnAction(eh -> {
			if (!this.modele.getEtat().getValue()) {
				if (!this.vue.getEdition().getProba().getTextF().getText().isBlank()) {
					int v = Integer.parseInt(this.vue.getEdition().getProba().getTextF().getText());
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("initialisation aléatoire de plateau");
					alert.setContentText(
							"Voulez-vous vraiment \n initialiser aléatoirement le plateau avec la probabilité " + v
									+ "%  ?");
					Optional<ButtonType> option = alert.showAndWait();
					if (option.get() == ButtonType.OK) {
						this.modele.initMatriceAlea(v);
						this.vue.refresh(this.modele.getMatriceJeu());
						this.vue.getEdition().getProba().getTextF().setText("");
					}
				}
			} else {
				this.AlertShow("avertissement", "il faut arrêter la simulation!");
			}
		});
		// met en action le Button reset
		this.vue.getEdition().getReset().setOnAction(eh -> {
			if (!this.modele.getEtat().getValue()) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("réinitialisation de plateau");
				alert.setContentText(
						"Voulez-vous vraiment \n réinitialiser tout le plateau en tuant toutes les cellules?");
				Optional<ButtonType> option = alert.showAndWait();
				if (option.get() == ButtonType.OK) {
					this.modele.initMatriceVide();
					this.vue.refresh(this.modele.getMatriceJeu());
					this.modele.getSleepTime().setValue(50);
					this.modele.getIter().setValue(0);
				}
			} else {
				this.AlertShow("avertissement", "il faut arrêter la simulation!");
			}

		});

		// Binder la valeur du slider vitesse, à la valeur de la propriété SleepTime
		this.vue.getEdition().getVitesse().valueProperty().bindBidirectional(this.modele.getSleepTime());

		// parametres
		//initialiser les valeurs des ComboBox du composant ParametreJeu, Binder leur valeurs à les propriété correspondant dans le model
		// morSolitude
		this.vue.getParametre().getComboMortSolitud().selectIndex(ModelJeuDeLaVie.getMortSolitudeIndex());
		this.vue.getParametre().getComboMortSolitud().disableProperty().bind(this.modele.getEtat());
		this.modele.getMortSolitude().bind(this.vue.getParametre().getComboMortSolitud().getValueProprerty());
		// Asphyxie
		this.vue.getParametre().getComboMortAsphyxie().selectIndex(ModelJeuDeLaVie.getMortAsphyxieIndex());
		this.vue.getParametre().getComboMortAsphyxie().disableProperty().bind(this.modele.getEtat());
		this.modele.getMortAsphyxie().bind(this.vue.getParametre().getComboMortAsphyxie().getValueProprerty());
		// viemin
		this.vue.getParametre().getComboVieMin().selectIndex(ModelJeuDeLaVie.getVieMinIndex());
		this.vue.getParametre().getComboVieMin().disableProperty().bind(this.modele.getEtat());
		this.modele.getVieMin().bind(this.vue.getParametre().getComboVieMin().getValueProprerty());
		// vieMax
		this.vue.getParametre().getComboVieMax().selectIndex(ModelJeuDeLaVie.getVieMaxIndex());
		this.vue.getParametre().getComboVieMax().disableProperty().bind(this.modele.getEtat());
		this.modele.getVieMax().bind(this.vue.getParametre().getComboVieMax().getValueProprerty());
			
		// met en action le button PlayPause
		this.vue.getPlayPause().setOnAction(eh -> {
			if(this.modele.isValid()) {
			this.modele.getEtat().setValue(!this.modele.getEtat().getValue());
			this.vue.getPlayPause().switchIcon();
			if (this.modele.getEtat().getValue())
				this.service.restart();
				
			}else
			{
				this.AlertShow("avertissement", this.modele.getProblemes());
			}
		});

		// Compteur : binder la valeur de propriété iteration du compteur, à la propriété itération du model
		this.vue.getCompteur().getIter().bind(this.modele.getIter());
		
		// Tampon
		// selectionner le premier motif prédéfini dans le choixMotif
		this.vue.getChoixMotf().initCombo(0);
		// met en action le button charger pour charger un motif, dans le zone  tampon
		this.vue.getChoixMotf().getCharger().setOnAction(e -> {
			this.modele.initTamponModel(Factory.getModel(this.vue.getChoixMotf().getSelectedModel()));
			this.vue.getTampon().refresh(this.modele.getMatriceTampon());
		});
		//ajouter les événements de clique, sur les cellules du Tampon
		for (int m = 0; m < this.vue.getTampon().getSize(); m++) {
			for (int n = 0; n < this.vue.getTampon().getSize(); n++) {
				final int x = m;
				final int y = n;
				this.vue.getTampon().getCellule(x, y).setOnMouseClicked(e -> {
					boolean b = this.modele.switchCelluleValeurTampon(x, y);
					this.vue.getTampon().setCell(x, y, b);

				});
			}
		}
	}

	/**
	 * @return la vue du jeu
	 */
	public VueJeuDeLaVie getVue() {
		return this.vue;
	}
	
	/**
	 * ajouter des evenements à chaque cellule du plateau du jeu principal
	 */
	public void addActionsClickPlateau() {
		for (int m = 0; m < this.vue.getPlateau().getSize(); m++) {
			for (int n = 0; n < this.vue.getPlateau().getSize(); n++) {
				final int x = m;
				final int y = n;
				this.vue.getPlateau().getCellule(x, y).setOnMouseClicked(e -> {
					if (!this.modele.getEtat().getValue()) {
						if (e.getButton() == MouseButton.SECONDARY) {

							if (e.isShiftDown()) {

								this.modele.insererTampon(x, y);
								this.vue.refresh(this.modele.getMatriceJeu());
							} else {
								this.modele.initTamponMatrice(x, y);
								this.vue.getTampon().refresh(this.modele.getMatriceTampon());
							}
						} else {
							boolean b = this.modele.switchCelluleValeurMatrice(x, y);
							this.vue.getPlateau().setCell(x, y, b);
						}
					} else {
						this.AlertShow("avertissement", "il faut arrêter la simulation!");
					}
				});
			}
		}
	}
	
	/**
	 * afficher une alerte
	 * @param title : titre de l'alerte
	 * @param header : contenu de l'alerte
	 */
	public void AlertShow(String title, String header) {
		Alert newAlert = new Alert(AlertType.WARNING);
		newAlert.setTitle(title);
		newAlert.setHeaderText(header);
		newAlert.show();
	}

	/**
	 * @return le ModelJeuDeLaVie du jeu
	 */
	public ModelJeuDeLaVie getModele() {
		return modele;
	}
	
	

}
