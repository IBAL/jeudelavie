package jeudelavie.controleur;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 * Un service lancé par le controlleur, pour evaluer le plateau
 *
 */
public class EvaluationService extends Service<JeuDeLaVieController>{

	public JeuDeLaVieController controller;
	public EvaluationService(JeuDeLaVieController controller) 
	{
		this.controller=controller;
	}
	@Override
	protected Task<JeuDeLaVieController> createTask() {
		return new EvaluationTask(this.controller);
	}
	
}
