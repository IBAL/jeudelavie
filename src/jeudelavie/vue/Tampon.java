package jeudelavie.vue;

import javafx.geometry.Pos;
import javafx.scene.layout.Pane;

public class Tampon extends Pane{
	public static final int CELL_TAMPON_SIZE = 17;
	private Board tamponBoard;
	
	/**
	 * Constructeur parametre du composant Tampon de size
	 * @param size : taille du matrice tampon
	 */
	public Tampon(int size) 
	{
		this(size,CELL_TAMPON_SIZE);
	}
	
	/**
	 * Constructeur parametre du composant Tampon de size
	 * @param size : taille du matrice tampon
	 * @param cellSize : taille des cellule
	 */
	public Tampon(int size,int cellSize) 
	{
		this.tamponBoard=new Board(size,cellSize);
		this.tamponBoard.setAlignment(Pos.CENTER);
		this.getChildren().add(this.tamponBoard);
	}
	
	/**
	 * @return cellule dans la position x, y
	 * @param x           la position en x de la case
	 * @param y           la position en y de la case
	 */
	public Cellule getCellule(int x,int y) {
		return this.tamponBoard.getCellule(x,y);
	}
	
	/**
	 * @return la taille du matrice tampon
	 */
	public int getSize() {
		return this.tamponBoard.getSize();
	}
	
	/**
	 * Place un caractére dans une case (x,y) avec une couleur de fond au format CSS
	 * 
	 * @param x           la position en x de la case
	 * @param y           la position en y de la case
	 * @param etat		 : true : vivante/ false : morte
	 */
	public void setCell(int x, int y, boolean etat) {

		this.tamponBoard.setCell(x, y, etat);
	}
	
	/**
	 * 
	 * Actualise le plateau tampon, par la matrice tampon passé en paremètre
	 * @param matriceTampon : matrice du jeu tampon
	 */
	public void refresh(boolean[][] matriceTampon) 
	{
		this.tamponBoard.refresh(matriceTampon);

	}
}
