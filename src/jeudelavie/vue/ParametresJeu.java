package jeudelavie.vue;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ParametresJeu extends VBox{
	
	private static final String LABEL_G  = "Paramètres de Jeu";
	private static final String LABEL_PS = "Mort Solitud";
	private static final String LABEL_PA = "Mort Asphyxie";
	private static final String LABEL_PVN = "Vie Minimum";
	private static final String LABEL_PVX = "Vie Maximum";
	private static final String BACKGROUND_COLOR_P = "#c8fac8";

	private static final int BORDER_WIDTH_P = 1;
	private static final int CORNER_RADIUS_P = 5;
	
	/**
	 * @return the comboMortAsphyxie
	 */
	public ComboBoxLabel getComboMortAsphyxie() {
		return comboMortAsphyxie;
	}

	/**
	 * @return the comboVieMin
	 */
	public ComboBoxLabel getComboVieMin() {
		return comboVieMin;
	}

	/**
	 * @return the comboVieMax
	 */
	public ComboBoxLabel getComboVieMax() {
		return comboVieMax;
	}

	private static final Color BORDER_COLOR_P = Color.DARKGREEN;
	
	private static final int SPACING = 8;
	
	private ComboBoxLabel comboMortSolitud;
	private ComboBoxLabel comboMortAsphyxie;
	private ComboBoxLabel comboVieMin;
	private ComboBoxLabel comboVieMax;
	
	/**
	 * constructeur paramétre du composant parametre du jeu
	 * @param mortSolitude : list des valeur possible pour le paramètre mort Solitude
	 * @param mortAsphyxie : list des valeur possible pour le paramètre mort Asphyxie
	 * @param vieMin : list des valeur possible pour le paramètre vie min
	 * @param vieMax : list des valeur possible pour le paramètre vie max
	 */
	public ParametresJeu(int[] mortSolitude ,int[] mortAsphyxie,int[] vieMin,int[] vieMax) 
	{
		Label par = new Label(LABEL_G);
		par.setAlignment(Pos.CENTER);
		par.setStyle(this.getStyle()+
			    "-fx-font-family: Courier New;"+
			    "-fx-font-weight: bold;"+
			    "-fx-font-size: 15;");
		HBox hp=new HBox(new Spring(),par,new Spring());

		this.comboMortSolitud=new ComboBoxLabel(LABEL_PS, mortSolitude);
		this.comboMortAsphyxie = new ComboBoxLabel(LABEL_PA, mortAsphyxie);
		this.comboVieMin=new ComboBoxLabel(LABEL_PVN,vieMin);
		this.comboVieMax=new ComboBoxLabel(LABEL_PVX,vieMax);
		
		this.setSpacing(SPACING);
		this.setStyle("-fx-background-color: " + BACKGROUND_COLOR_P + ";");
		this.setBorder(new Border(new BorderStroke(BORDER_COLOR_P, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADIUS_P), new BorderWidths(BORDER_WIDTH_P))));
	
		
		this.getChildren().addAll(hp,this.comboMortSolitud,this.comboMortAsphyxie,this.comboVieMin,this.comboVieMax);
		
	}

	/**
	 * @return the comboMortSolitud
	 */
	public ComboBoxLabel getComboMortSolitud() {
		return comboMortSolitud;
	}

}
