package jeudelavie.vue;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

//Ceci n'est qu'un test pour tester les différents composants de la vue générale, il ne fait pas partie de l'application
public class TestVue extends Application {
	@Override
	public void start(Stage primaryStage) {

		/*
		 * BorderPane root=new BorderPane(); Board b = new Board(10); b.setCell(1,3,
		 * true); b.setCell(3,3, true); b.setCell(4,2, true);
		 */
		// ButtonPlayPause b1=new ButtonPlayPause();
		// root.setTop(b1);
		// Compteur c= new Compteur();
		// root.setTop(c);
		/*
		 * c.setOnMouseClicked(e -> { c.getIter().setValue(c.getIter().getValue()+1);
		 * 
		 * });
		 */
		/*
		 * int[] d= {1,2}; ParametresJeu p = new ParametresJeu(d,d,d,d);
		 * root.setLeft(p); EditionPlateau P2 = new EditionPlateau(); root.setRight(P2);
		 * root.setCenter(b); root.setBottom(new QuitButton());
		 */
		int[] d1 = { 1, 2 };
		int[] d2 = { 3, 4 };
		int[] d3 = { 3, 4 };
		int[] d4 = { 3, 4 ,5};
		String[] s = { "Gle2", "gle3" };
		VueJeuDeLaVie root = new VueJeuDeLaVie(25,10,d1, d2, d3, d4, s);
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}
}
