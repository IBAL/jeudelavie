package jeudelavie.vue;

import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;


public class Plateau extends ScrollPane{
	public static final int TAILLE_MIN = 200;
	private Board jeuBoard;
	
	/**
	 * construteur parametre du composant plateau principale
	 * @param size : la taille du plateau principale
	 */
	public Plateau(int size) 
	{
		this.jeuBoard= new Board(size);
		this.jeuBoard.setMinSize(TAILLE_MIN, TAILLE_MIN);
		this.jeuBoard.setAlignment(Pos.CENTER);
		this.setContent(this.jeuBoard);
		this.setPannable(true);
	}

	/**
	 * @return the jeuBoard
	 */
	public Board getJeuBoard() {
		return jeuBoard;
	}
	
	/**
	 * Actualise le plateau principale par la matrice principale
	 * @param matriceJeu : matrice du jeu principale
	 */
	public void refresh(boolean[][] matriceJeu) 
	{
		this.jeuBoard.refresh(matriceJeu);
		this.jeuBoard.setAlignment(Pos.CENTER);
	}
	
	/**
	 * @return the size
	 */
	public int getSize() {
		return this.jeuBoard.getSize();
	}
	
	/**
	 * @return cellule dans la position x, y
	 * @param x           la position en x de la case
	 * @param y           la position en y de la case
	 */
	public Cellule getCellule(int x,int y) {
		return this.jeuBoard.getCellule(x,y);
	}
	
	/**
	 * Place un caractére dans une case (x,y) avec une couleur de fond au format CSS
	 * 
	 * @param x           la position en x de la case
	 * @param y           la position en y de la case
	 * @param etat		 : true : vivante/ false : morte
	 */
	public void setCell(int x, int y, boolean etat) {

		this.jeuBoard.setCell(x, y, etat);
	}
}
