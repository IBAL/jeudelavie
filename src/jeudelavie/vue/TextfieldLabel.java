package jeudelavie.vue;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class TextfieldLabel extends HBox {
	
	private static final String BACKGROUND_C = "#fff";
	private static final String BORDER_COLOR_B = "#36b52f";
	private static final int SPACING = 9;
	private static final int PADDING_INSETS = 3;
	private static final int PREF_WIDTH = 50;
	private static final Color BORDER_COLOR = Color.BLACK;
	private static final int CORNER_RADII_HB = 2;
	private static final int BORDER_WIDTH = 1;
	private static final int CORNER_RADII_TF = 1;
	private static final String ICON_REFRESH = "icons/refresh.png";
	private static final int ICON_SIZE = 27;
	private static final int BUTTON_SIZE = 27;
	private Label etiquet;
	private TextField textF;
	private Button submit;
	
	/**
	 * Construit un composant, contenant un TextField, un Label et un Button
	 * @param txtLabel : text du label
	 */
	public TextfieldLabel(String txtLabel)
	{
		this.etiquet = new Label(txtLabel);
		this.textF = new TextField();
		Spring ressort = new Spring();
		
		this.submit = new Button();
		Image i = new Image("file:" + ICON_REFRESH);
		ImageView icon = new ImageView(i);
		icon.setFitHeight(ICON_SIZE);
		icon.setFitWidth(ICON_SIZE);
		icon.setPreserveRatio(true);
		icon.setSmooth(true);
		
		this.submit.setPrefSize(BUTTON_SIZE, BUTTON_SIZE);
		this.submit.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
		this.submit.setMaxSize(BUTTON_SIZE, BUTTON_SIZE);
		this.submit.setGraphic(icon);
		this.submit.setStyle("-fx-border-color: "+BORDER_COLOR_B+"");

		this.setSpacing(SPACING);
		this.setPadding(new Insets(PADDING_INSETS));
		this.textF.setPrefWidth(PREF_WIDTH);
		
		this.setBorder(new Border(new BorderStroke(BORDER_COLOR, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADII_HB), new BorderWidths(BORDER_WIDTH))));
		this.textF.setBorder(new Border(new BorderStroke(BORDER_COLOR, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADII_TF), new BorderWidths(BORDER_WIDTH))));
		this.setStyle("-fx-background-color: " + BACKGROUND_C + ";");
		
		this.getChildren().addAll(this.etiquet,ressort, this.textF,this.submit);
	}
	/**
	 * @return the textF
	 */
	public TextField getTextF() {
		return textF;
	}
	/**
	 * @return the submit
	 */
	public Button getSubmit() {
		return submit;
	}
	
}
