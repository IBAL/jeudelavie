package jeudelavie.vue;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class Cellule extends Label {

	private static final String E_BACKGROUND = "#fff";
	private static final String F_BACKGROUND = "#000";
	public static final int CELL_SIZE = 5;
	private static final Color BORDER_COLOR_CELL = Color.BLACK;
	private static final int BORDER_WIDTH_CELL = 1;
	private static final int CORNER_RADIUS_CELL = 0;

	/**
	 * construit une cellule du plateau de taille
	 * 
	 * @param cellSize : taille de la cellule
	 */
	public Cellule(int cellSize) {
		this(cellSize, false);
	}

	/**
	 * construit une cellule du plateau de taille et à une état
	 * 
	 * @param cellSize : taille de la cellule
	 * @param etat     : true : vivante / false : morte
	 */
	public Cellule(int cellSize, boolean etat) {
		this.setPrefSize(cellSize, cellSize);
		this.setMaxSize(cellSize, cellSize);
		this.setMinSize(cellSize, cellSize);
		this.setAlignment(Pos.CENTER);
		if (etat)
			this.setStyle("-fx-background-color: " + F_BACKGROUND + ";");
		else
			this.setStyle("-fx-background-color: " + E_BACKGROUND + ";");
		this.setBorder(new Border(new BorderStroke(BORDER_COLOR_CELL, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADIUS_CELL), new BorderWidths(BORDER_WIDTH_CELL))));

	}

	/**
	 * modifie l'etat une cellule
	 * @param etat boolean indique, que la cellule soit vivant soit non
	 */
	public void setCell(boolean etat) {
		// cellule noir si elle est vivante, blanc sinon
		String clr = etat ? F_BACKGROUND : E_BACKGROUND;
		this.setStyle("-fx-background-color: " + clr + ";");
	}

	/**
	 * reinitialiser tout les cellule du plateau à l'état mort
	 */
	public void resetCell() {
		this.setStyle("-fx-background-color: " + E_BACKGROUND + ";");
	}

}
