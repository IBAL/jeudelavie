package jeudelavie.vue;

import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class VueJeuDeLaVie extends BorderPane {

	private Plateau plateau;

	private EditionPlateau edition;
	private ParametresJeu parametre;
	private ButtonPlayPause playPause;
	private Compteur compteur;
	private Tampon tampon;
	private ChoixMotif choixMotf;
	private QuitButton quit;

	/**
	 * Construit l'interface globale de l'application
	 * @param taillePlateau : la taille du plateau principale
	 * @param tailleTampon : la taille du plateau tampon
	 * @param mortSolitude : list des valeur possible pour le paramètre mort Solitude
	 * @param mortAsphyxie : list des valeur possible pour le paramètre mort Asphyxie
	 * @param vieMin : list des valeur possible pour le paramètre vie min
	 * @param vieMax : list des valeur possible pour le paramètre vie max
	 * @param models : list de models prédéfinis
	 */
	public VueJeuDeLaVie(int taillePlateau, int tailleTampon, int[] mortSolitude, int[] mortAsphyxie, int[] vieMin,
			int[] vieMax, String[] models) {
		this.plateau = new Plateau(taillePlateau);
		this.edition = new EditionPlateau();
		this.parametre = new ParametresJeu(mortSolitude, mortAsphyxie, vieMin, vieMax);
		this.playPause = new ButtonPlayPause();
		this.compteur = new Compteur();
		this.tampon = new Tampon(tailleTampon);
		this.choixMotf = new ChoixMotif(models);
		this.quit = new QuitButton();

		this.setCenter(plateau);

		VBox left = new VBox();
		HBox left_h1 = new HBox(new Spring(), this.playPause, new Spring(), this.compteur, new Spring());

		left.setSpacing(5);
		left.getChildren().addAll(this.edition, this.parametre, new Spring(), left_h1, new Spring());

		VBox right = new VBox();
		right.setSpacing(5);
		HBox right_h1 = new HBox(new Spring(), this.quit);
		right.getChildren().addAll(this.tampon, this.choixMotf, new Spring(), right_h1);

		this.setLeft(left);
		this.setRight(right);
		Insets insets = new Insets(10);

		BorderPane.setMargin(right, insets);
		BorderPane.setMargin(left, insets);
		BorderPane.setMargin(this.plateau, insets);
	}

	/**
	 * @return l'edition plateau
	 */
	public EditionPlateau getEdition() {
		return edition;
	}

	/**
	 * Actualise le plateau principale par la matrice principale
	 * @param matriceJeu : matrice du jeu principale
	 */
	public void refresh(boolean[][] matriceJeu) {
		this.plateau.refresh(matriceJeu);

	}

	/**
	 * @return le composant paramètre de jeu
	 */
	public ParametresJeu getParametre() {
		return parametre;
	}

	/**
	 * @return le composant Tampon
	 */
	public Tampon getTampon() {
		return tampon;
	}

	/**
	 * @return le Button PlayPause
	 */
	public ButtonPlayPause getPlayPause() {
		return playPause;
	}

	/**
	 * @return le plateau
	 */
	public Plateau getPlateau() {
		return plateau;
	}

	/**
	 * @return le Button quit
	 */
	public QuitButton getQuit() {
		return quit;
	}

	/**
	 * @return le composant choixMotf
	 */
	public ChoixMotif getChoixMotf() {
		return choixMotf;
	}

	/**
	 * @return le composant compteur
	 */
	public Compteur getCompteur() {
		return compteur;
	}
}
