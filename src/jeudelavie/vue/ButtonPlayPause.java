package jeudelavie.vue;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ButtonPlayPause extends Button {

	private BooleanProperty etat;

	private static final String ICON_PAUSE = "icons/pause.png";
	private static final String ICON_PLAY = "icons/play.png";

	private static final int ICON_SIZE = 75;
	private static final int BUTTON_SIZE = 80;
	
	/**
	 * construit un Button play pause, qui se change mutuellement après chaque clique 
	 */
	public ButtonPlayPause() {
		this.etat = new SimpleBooleanProperty(false);
		Image i = new Image("file:" + ICON_PLAY);
		ImageView icon = new ImageView(i);
		icon.setFitHeight(ICON_SIZE);
		icon.setFitWidth(ICON_SIZE);
		this.setPrefSize(BUTTON_SIZE, BUTTON_SIZE);
		this.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
		this.setMaxSize(BUTTON_SIZE, BUTTON_SIZE);
		icon.setPreserveRatio(true);
		icon.setSmooth(true);
		this.setGraphic(icon);
		this.setBackground(null);


	}

	/**
	 * change les icon play, pause dans le Button playpause
	 */
	public void switchIcon() {
		Image i;
		if (!this.etat.getValue()) {
			i = new Image("file:" + ICON_PAUSE);
			this.etat.setValue(true);
		} else {
			i = new Image("file:" + ICON_PLAY);
			this.etat.setValue(false);
		}
		ImageView icon = new ImageView(i);
		
		icon.setFitHeight(ICON_SIZE);
		icon.setFitWidth(ICON_SIZE);
		icon.setPreserveRatio(true);
		icon.setSmooth(true);
		this.setGraphic(icon);
	}

}
