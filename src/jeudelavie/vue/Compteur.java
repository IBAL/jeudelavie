package jeudelavie.vue;

import javafx.beans.property.IntegerProperty;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;

import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;


public class Compteur extends Label {

	private IntegerProperty iter;
	private static final String L_BACKGROUND = "#f2d48f";
	private static final Color BORDER_COLOR_CELL = Color.BLACK;
	private static final int BORDER_WIDTH = 3;
	private static final int CORNER_RADIUS = 5;

	private static final int LABEL_SIZE = 80;

	/**
	 * Construit un compteur d'itérations du jeu
	 */
	public Compteur() {
		this.iter = new SimpleIntegerProperty(0);
		
		this.setPrefSize(LABEL_SIZE, LABEL_SIZE);
		this.setMinSize(LABEL_SIZE, LABEL_SIZE);
		this.setMaxSize(LABEL_SIZE, LABEL_SIZE);
		this.setStyle("-fx-background-color: " + L_BACKGROUND + ";");
		this.setBorder(new Border(new BorderStroke(BORDER_COLOR_CELL, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADIUS), new BorderWidths(BORDER_WIDTH))));
		
		this.setStyle(this.getStyle()+
			    "-fx-font-family: Courier New;"+
			    "-fx-font-weight: bold;"+
			    "-fx-font-size: 25;");
		this.setAlignment(Pos.CENTER);
		this.textProperty().bind(this.iter.asString());
	}

	/**
	 * un getter sur la propriété des iterations du jeu
	 * @return the iter
	 */
	public IntegerProperty getIter() {
		return iter;
	}

}
