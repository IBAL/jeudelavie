package jeudelavie.vue;

import javafx.geometry.Pos;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class Board extends GridPane {

	private static final int GAP = 0;
	private static final int PADDING = 13;
	private static final Color BORDER_COLOR = Color.BLACK;
	private static final int BORDER_WIDTH = 3;
	private static final int CORNER_RADIUS = 5;

	private int size;
	private Cellule cellules[][];

	/**
	 * Construit une grille de taille size
	 * @param size la taille du plateau
	 */
	public Board(int size) {
		this(size, Cellule.CELL_SIZE);

	}

	/**
	 * Construit une grille de taille size
	 * @param size la taille du plateau
	 * @param cellSize : taille des cellule
	 */
	public Board(int size, int celSize) {
		super();
		this.size = size;
		this.setVgap(GAP);
		this.setHgap(GAP);
		this.setAlignment(Pos.CENTER);

		this.setStyle("-fx-padding: " + PADDING + ";");
		cellules = new Cellule[size][size];
		// remplissage des cases avec un panneau fond CELLCOLOR
		for (int k = 0; k < size; k++) {
			for (int j = 0; j < size; j++) {
				Cellule c = new Cellule(celSize);

				this.add(c, j, k);
				cellules[k][j] = c;
			}
		}

		// Création de la bordure
		this.setBorder(new Border(new BorderStroke(BORDER_COLOR, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADIUS), new BorderWidths(BORDER_WIDTH))));
	}

	/**
	 * Place un caractère dans une case (x,y) avec une couleur de fond au format CSS
	 * @param x    la position en x de la case
	 * @param y    la position en y de la case
	 * @param etat : true : vivante / false : morte
	 */
	public void setCell(int x, int y, boolean etat) {

		if ((x >= 0 && x < size) && (y >= 0 && y < size)) {
			cellules[x][y].setCell(etat);
		}
	}

	/**
	 * Réinitialise (à vide) la case (x,y) à la couleur
	 * @param x : position ligne
	 * @param y : position colonne
	 */
	public void resetCell(int x, int y) {
		if ((x >= 0 && x < size) && (y >= 0 && y < size)) {
			cellules[x][y].resetCell();
		}
	}

	/**
	 * Actualise grille par la matrice du jeu (utilisé pour actualiser les états des cellules / modifier la taille de la grille )
	 * @param matriceJeu : matrice du jeu
	 */
	public void refresh(boolean[][] matriceJeu) {

		int newSize = matriceJeu.length;
		if (this.size == newSize) {
			refreshVieMort(matriceJeu);
		} else {
			this.reScale(newSize);
		}
	}
	
	/**
	 * modifier la taille du grille, mise à jour des cellules (ajout, suppression)
	 * @param newSize : nouvelle taille de la grille
	 */
	private void reScale(int newSize) {
		Cellule[][] tempC = new Cellule[newSize][newSize];
		if (newSize < this.size) {
			for (int k = 0; k < newSize; k++) {
				for (int j = 0; j < newSize; j++) {
					tempC[k][j] = this.cellules[k][j];
				}
			}
			for (int i = newSize; i < size; i++) {
				final int tmp = i;
				this.getChildren().removeIf(node -> GridPane.getRowIndex(node) == tmp);
				this.getChildren().removeIf(node -> GridPane.getColumnIndex(node) == tmp);
			}
		} else {
			for (int k = 0; k < this.size; k++) {
				for (int j = 0; j < this.size; j++) {

					tempC[k][j] = this.cellules[k][j];
				}
			}
			for (int k = 0; k < newSize; k++) {
				for (int j = this.size; j < newSize; j++) {
					Cellule c = new Cellule(Cellule.CELL_SIZE);
					this.add(c, j, k);
					tempC[k][j] = c;
				}
			}
			for (int k = this.size; k < newSize; k++) {
				for (int j = 0; j < newSize; j++) {
					Cellule c = new Cellule(Cellule.CELL_SIZE);
					this.add(c, j, k);
					tempC[k][j] = c;
				}
			}
		}
		this.cellules = tempC;
		this.size = newSize;

	}

	/**
	 * Actualise le plateau principale par la matrice principale
	 * @param matriceJeu : matrice du jeu principale
	 */
	public void refreshVieMort(boolean[][] matriceJeu) {

		// remplissage des cases avec un panneau fond CELLCOLOR
		for (int k = 0; k < size; k++) {
			for (int j = 0; j < size; j++) {
				this.setCell(k, j, matriceJeu[k][j]);
			}
		}
	}

	/**
	 * retoun la une cellule dans une position x,y
	 * @param x : position ligne
	 * @param y : position colonne
	 */
	public Cellule getCellule(int x, int y) {
		return cellules[x][y];
	}

	/**
	 * @return la taille de la grille
	 */
	public int getSize() {
		return size;
	}

}
