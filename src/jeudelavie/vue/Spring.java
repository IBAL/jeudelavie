package jeudelavie.vue;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * Une classe ressort pour espacer dans les Box
 */
public class Spring extends Region {
	
	/**
	 * construit un ressort
	 */
	public Spring() {
		this.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(this, Priority.ALWAYS);
		this.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(this, Priority.ALWAYS);
	}

}
