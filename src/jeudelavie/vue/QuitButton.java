package jeudelavie.vue;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class QuitButton extends Button {
	private static final String TEXT = "QUIT";
	private static final Color BORDER_COLOR = Color.BLACK;
	private static final Color BACKGROUND_COLOR = Color.ORANGERED;
	private static final int BORDER_WIDTH = 1;
	private static final int CORNER_RADIUS = 5;
	private static final int PREF_HIGHT = 30;
	private static final int PREF_WIDTH = 80;
	
	/**
	 * construit un Button Quit pour quitter l'application 
	 */
	public QuitButton() 
	{
		this.setText(TEXT);
		this.setBackground(new Background(new BackgroundFill(BACKGROUND_COLOR, new CornerRadii(CORNER_RADIUS), Insets.EMPTY)));
		this.setBorder(new Border(new BorderStroke(BORDER_COLOR, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADIUS), new BorderWidths(BORDER_WIDTH))));
		this.setPrefHeight(PREF_HIGHT);
		this.setPrefWidth(PREF_WIDTH);
		this.setStyle("-fx-font-weight: bold;"+
			    "-fx-font-size: 15;");
	} 

}
