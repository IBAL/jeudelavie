package jeudelavie.vue;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class EditionPlateau extends VBox{
	
	
	private static final String EDITPLATEAU_BC = "#fab89d";
	private static final String BACKGROUND_RESET = "#317af7";
	private static final Color EDITPLATEAU_BRC = Color.DARKRED;
	private static final int BORDER_WIDTH_EP = 1;
	private static final int CORNER_RADIUS_EP = 5;
	
	private static final String TXT_PLATEAU = "Edition Plateau";
	private static final String TXT_RESET = "Reinitialiser";
	private static final String TXT_ALEA = "Init Aleatoir";
	private static final String TXT_TAILLE = "Taille Plateau";
	private static final int MARGIN_RESET = 10;
	

	private TextfieldLabel taille;
	private TextfieldLabel proba;
	private Button reset;
	private Slider vitesse;
	
	private static final int VITESSE_MIN =  0;
	private static final int VITESSE_MAX = 100;
	private static final int VITESSE_DEFAULT = 50;
	
	/**
	 * construit un composant pour : changer la taille du plateau, choisir une initialisation aleatoir avec une probailité choisi
	 */
	public EditionPlateau()
	{
		this.taille = new TextfieldLabel(TXT_TAILLE);
		this.proba = new TextfieldLabel(TXT_ALEA);
		Label titre = new Label(TXT_PLATEAU);
		titre.setAlignment(Pos.CENTER);
		Label vitesseLabel = new Label("Vitesse");
		vitesseLabel.setStyle(this.getStyle()+
			    "-fx-font-family: Courier New;"+
			    "-fx-font-weight: bold;"+
			    "-fx-font-size: 12;");
		vitesseLabel.setAlignment(Pos.CENTER);
		this.reset = new Button(TXT_RESET);
		this.vitesse=new  Slider(VITESSE_MIN,VITESSE_MAX,VITESSE_DEFAULT);
		this.vitesse.setShowTickMarks(true);
		this.vitesse.setShowTickLabels(true);
		this.vitesse.setBlockIncrement(10);
		
		titre.setStyle(this.getStyle()+
			    "-fx-font-family: Courier New;"+
			    "-fx-font-weight: bold;"+
			    "-fx-font-size: 15;");
		this.setStyle("-fx-background-color: " + EDITPLATEAU_BC + ";");
		this.setBorder(new Border(new BorderStroke(EDITPLATEAU_BRC, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADIUS_EP), new BorderWidths(BORDER_WIDTH_EP))));
		this.setSpacing(7);
		this.reset.setStyle("-fx-background-color: " + BACKGROUND_RESET + ";"+
				"-fx-font-weight: bold;"+
				"-fx-text-fill: white"
				);
		
		HBox.setMargin(this.reset, new Insets(MARGIN_RESET));
		HBox h1= new HBox(new Spring(),titre, new Spring());
		HBox h2= new HBox(new Spring(),vitesseLabel, new Spring());
		HBox h3= new HBox(new Spring(),this.reset, new Spring());
		
		this.getChildren().addAll(h1,this.taille,this.proba,h2,this.vitesse, h3);
	}
	
	/**
	 * @return le composant taille
	 */
	public TextfieldLabel getTaille() {
		return taille;
	}

	/**
	 * @return le composant proba
	 */
	public TextfieldLabel getProba() {
		return proba;
	}

	/**
	 * @return le composant reset
	 */
	public Button getReset() {
		return reset;
	}

	/**
	 * @return le composant vitesse
	 */
	public Slider getVitesse() {
		return vitesse;
	}

}
