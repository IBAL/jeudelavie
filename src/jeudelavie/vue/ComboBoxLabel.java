package jeudelavie.vue;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class ComboBoxLabel extends HBox {
	private static final int BORDER_WIDTH_C = 1;
	private static final int CORNER_RADIUS_C = 2;
	private static final Color BORDER_COLOR_C = Color.BLACK;
	private static final int PREF_WITH_COM = 100;
	private static final int PADDING = 5;
	private static final int MARGIN_RIGHT = 3;
	private static final String BACKGROUND_COLOR = "#fff";
	private ComboBox<Integer> comboBox;
	private Label comboName;
	private int[] comboValues;
	private IntegerProperty value;

	/**
	 * construit un composant contenant à la fois un ComboBox, et un label
	 * 
	 * @param name   est le txt du label
	 * @param values sont les valeurs porté par le ComboBox
	 */
	public ComboBoxLabel(String name, int[] values) {
		this.comboName = new Label(name);
		this.comboBox = new ComboBox<>();
		this.comboValues = new int[values.length];
		for (int i = 0; i < values.length; i++) {
			this.comboValues[i] = values[i];
			this.comboBox.getItems().add(values[i]);
		}
		this.comboBox.setPrefWidth(BORDER_WIDTH_C);
		this.setStyle("-fx-background-color: " + BACKGROUND_COLOR + ";");
		this.setBorder(new Border(new BorderStroke(BORDER_COLOR_C, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADIUS_C), new BorderWidths(BORDER_WIDTH_C))));
		this.setPadding(new Insets(PADDING));
		this.comboBox.setPrefWidth(PREF_WITH_COM);
		this.getChildren().addAll(this.comboName, new Spring(), this.comboBox);
		HBox.setMargin(comboName, new Insets(1, MARGIN_RIGHT, 1, 1));
		this.value = new SimpleIntegerProperty();
		this.comboBox.getSelectionModel().selectedItemProperty().addListener((composant, oldValue, newValue) -> {
			this.value.setValue(newValue);
			
		});
	}
	
	/**
	 * @return la propriété value du ComboBoxLabel 
	 */
	public IntegerProperty getValueProprerty() {
		return value;
	}

	/**
	 * @return le comboBox
	 */
	public ComboBox<Integer> getComboBox() {
		return comboBox;
	}
	
	/**
	 * selectionnet une valeur à l'index idx, du ComboBox
	 * @param idx : l'index
	 */
	public void selectIndex(int idx) 
	{
		this.comboBox.getSelectionModel().select(idx);
	}
}
