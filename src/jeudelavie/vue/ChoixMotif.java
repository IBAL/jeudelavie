package jeudelavie.vue;



import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ChoixMotif extends VBox{
	
	private static final String BUTTON_TXT = "Charger";
	private static final int BORDER_WIDTH_BUTTON = 1;
	private static final int CORNER_RADIUS_BUTTON = 5;
	private static final Color BORDER_COLOR_BUTTON = Color.BLACK;
	private static final String BACKGROUND_COLOR__BUTTON = "#05f76e";
	private static final int PREF_WITH_COM = 120;
	private static final int SPACING = 5;
	
	private Button charger;
	private ComboBox<String> comboMotif;
	private String[] listChoix;
	
	/**
	 * construit un composant pour choisir un jeux prédifinis
	 * @param listChoix list des jeux prédifinis
	 */
	public ChoixMotif(String[] listChoix)
	{
		//construction d'un HBox pour contenir le button charger
		HBox hb = new HBox();
		this.listChoix = new String[listChoix.length];
		Color BG_BTN = Color.web(BACKGROUND_COLOR__BUTTON);
		this.charger = new Button(BUTTON_TXT);
		this.charger.setBackground(new Background(new BackgroundFill(BG_BTN, new CornerRadii(CORNER_RADIUS_BUTTON), Insets.EMPTY)));
		this.charger.setBorder(new Border(new BorderStroke(BORDER_COLOR_BUTTON, BorderStrokeStyle.SOLID,
				new CornerRadii(CORNER_RADIUS_BUTTON), new BorderWidths(BORDER_WIDTH_BUTTON))));
		
		//charger un ressort et le button charger, le HBox pour aligner le button à droite dans le composant principal
		hb.getChildren().addAll(new Spring(), this.charger);
		this.comboMotif = new ComboBox<>();
		for (int i = 0; i<listChoix.length; i++) {
			this.comboMotif.getItems().add(listChoix[i]);
			this.listChoix[i] = listChoix[i];
		}
		this.setSpacing(SPACING);
		this.comboMotif.setPrefWidth(PREF_WITH_COM);
		this.setAlignment(Pos.TOP_RIGHT);
		this.getChildren().addAll(comboMotif, hb);
		
		
	}

	/**
	 * @return le Button charger
	 */
	public Button getCharger() {
		return charger;
	}

	/**
	 * @return le composant comboMotif
	 */
	public ComboBox<String> getComboMotif() {
		return comboMotif;
	}
	
	/**
	 * initialisation : selection d'une valeur du ComboBox, à l'index i
	 * @param i : l'index
	 */
	public void initCombo(int i) 
	{
		if(i<this.listChoix.length)
			this.comboMotif.getSelectionModel().select(i);
				
	}
	
	/**
	 * @return la valeur selectionné par le ComboBox
	 */
	public String getSelectedModel() {
		return this.comboMotif.getSelectionModel().getSelectedItem();
	}
	
	

}
