package jeudelavie.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class ModelJeuDeLaVie {


	/**
	 * Taille matrice maximal
	 */
	private static final int TAILLE_MATRICE_MAX = 150;
	
	/**
	 * Taille tampon maximal
	 */
	private static final int TAILLE_TAMPON_MAX = 10;
	
	/**
	 * valeur par defaut du paramètre mortSolitude
	 */
	private static final int MORT_SOLITUDE = 1;
	
	/**
	 * valeur par defaut du paramètre mortAsphyxie
	 */
	private static final int MORT_ASPHYXIE = 4;
	
	/**
	 * valeur de parametre vieMin par defaut
	 */
	private static final int VIE_MIN = 3;
	
	/**
	 * valeur de parametre vieMax par defaut
	 */
	private static final int VIE_MAX = 3;
	
	/**
	 * valeur par defaut de la propriété sleepTime
	 */
	private static final double SLEEP_TIME = 50;

	/**
	 * matrice representant l'etat des cellules
	 */
	private MatriceJeu matrice;

	/**
	 * matrice representant le tampon
	 */
	private MatriceTampon tampon;

	/**
	 * la propriété qui present le parametre mortSolitude
	 */
	private IntegerProperty mortSolitude;
	/**
	 * la propriété qui present le parametre mortAsphyxie
	 */
	private IntegerProperty mortAsphyxie;
	/**
	 * la propriété qui present le parametre vieMin
	 */
	private IntegerProperty vieMin;
	/**
	 * la propriété qui present le parametre vieMax
	 */
	private IntegerProperty vieMax;

	/**
	 * la propriété representant le nombre d'iteration
	 */
	private IntegerProperty iter;

	/**
	 * la propriété qui reprepresente le sleep time
	 */
	private DoubleProperty sleepTime;

	/**
	 * la propriété qui reprepresente l'etat de jeu start= true /pause =false
	 */
	private BooleanProperty etat;

	private static final int[] MORT_SOLITUDE_LISTE = {0,MORT_SOLITUDE,2,3};
	private static final int[] MORT_ASPHYXIE_LISTE = {2,3,MORT_ASPHYXIE,5};
	private static final int[] VIE_MIN_LISTE = {0,1,2,VIE_MIN};
	private static final int[] VIE_MAX_LISTE = {1,2,VIE_MAX,4,5};
	
	private static final int MORT_SOLITUDE_INDEX =1 ;
	private static final int MORT_ASPHYXIE_INDEX = 2;
	private static final int VIE_MIN_INDEX = 3;
	private static final int VIE_MAX_INDEX = 2;
	
	/**
	 * constrecteur par defaut
	 */
	public ModelJeuDeLaVie() {
		this(MatriceJeu.TAILLE_PAR_DEFAUT, MatriceTampon.TAILLE_PAR_DEFAUT);

	}

	/**
	 * construction d'une matrice jeu de la vie de taille
	 * @param taille : taille de matrice
	 */
	public ModelJeuDeLaVie(int taille) {
		this(taille, MatriceTampon.TAILLE_PAR_DEFAUT);
	}

	/**
	 * construction d'une matrice jeu de la vie de taille, et de taille tampon
	 * @param taille : taille de la matrice du jeu de la vie
	 * @param tailleTampon : taille de la matrice tampon
	 */
	public ModelJeuDeLaVie(int taille, int tailleTampon) {
		this(taille, tailleTampon, MORT_SOLITUDE, MORT_ASPHYXIE, VIE_MIN, VIE_MAX);
	}

	/**
	 * construction d'une matrice jeu de la vie de taille, de taille tampon,
	 * d'un nombre de mort solitude, d'un nombre de mort Asphyxie, de vie min, et de vie max
	 * @param taille
	 * @param tailleTampon
	 * @param mortSolitude
	 * @param mortAsphyxie
	 * @param vieMin
	 * @param vieMax
	 */
	public ModelJeuDeLaVie(int taille, int tailleTampon, int mortSolitude, int mortAsphyxie, int vieMin, int vieMax) {
		if(taille>ModelJeuDeLaVie.TAILLE_MATRICE_MAX)
			taille= ModelJeuDeLaVie.TAILLE_MATRICE_MAX;
		if(tailleTampon>ModelJeuDeLaVie.TAILLE_TAMPON_MAX)
			tailleTampon= ModelJeuDeLaVie.TAILLE_TAMPON_MAX;
		this.iter = new SimpleIntegerProperty();
		this.mortAsphyxie = new SimpleIntegerProperty();
		this.mortSolitude = new SimpleIntegerProperty();
		this.vieMin = new SimpleIntegerProperty();
		this.vieMax = new SimpleIntegerProperty();
		this.sleepTime = new SimpleDoubleProperty();
		this.etat = new SimpleBooleanProperty();
		this.matrice = new MatriceJeu(taille);
		this.tampon = new MatriceTampon(tailleTampon);
		this.matrice.initMatriceVide();
		this.mortSolitude.setValue(mortSolitude);
		this.mortAsphyxie.setValue(mortAsphyxie);
		this.vieMin.setValue(vieMin);
		this.vieMax.setValue(vieMax);
		this.iter.setValue(0);
		this.etat.setValue(false);
		this.sleepTime.setValue(ModelJeuDeLaVie.SLEEP_TIME);
	}

	/**
	 * manipuler l'état d'une cellule précise
	 * @param x: position dans la ligne
	 * @param y : position dans la colonne
	 * @param valeur : l'etat d'une cellule
	 */
	public void setCelluleValeur(int x, int y, boolean valeur) throws Exception {
		if (!this.etat.getValue())
			this.matrice.setCelluleValeur(x, y, valeur);
	}

	/**
	 * récupérer dans un tableau de booléens une image du plateau
	 */
	public boolean[][] getMatriceJeu() {

		return this.matrice.getMatrice();
	}
	/**
	 * récupérer dans un tableau de booléens image tampon
	 */
	public boolean[][] getMatriceTampon() {

		return this.tampon.getMatrice();
	}

	/**
	 * initialiser le tableau : avec des cases vides
	 */
	public void initMatriceVide() {
		if (!this.etat.getValue())
			this.matrice.initMatriceVide();
	}

	/**
	 * initialiser le tableau : avec un tableau de booléens de la même dimension
	 *  
	 * @throws Exception
	 */
	public void initMatriceCopy(boolean[][] newMatrice) throws Exception {
		if (!this.etat.getValue())
			this.matrice.initMatriceCopy(newMatrice);

	}

	/**
	 * initialiser le tableau : aléatoirement avec une certaine probabilité
	 * 
	 * @param proba :probabilité( percentage % ) :probabilité qu'une case soit
	 *              occupée par une cellule vivante
	 */
	public void initMatriceAlea(int proba) {
		if (!this.etat.getValue())
			this.matrice.initMatriceAlea(proba);
	}

	/**
	 * modifier la taille de matrice
	 * 
	 * @param newSize: nouvelle taille de la matrice
	 */
	public void modifierTaille(int newSize) {
		
		if (!this.etat.getValue() && newSize <= ModelJeuDeLaVie.TAILLE_MATRICE_MAX)
			this.matrice.modifierTaille(newSize);
	}
	
	/**
	 * getter sur la propriété mortSolitude
	 */
	public IntegerProperty getMortSolitude() {
		return mortSolitude;
	}
	
	/**
	 * getter sur la propriété mortAsphyxie
	 */
	public IntegerProperty getMortAsphyxie() {
		return mortAsphyxie;
	}

	/**
	 * getter sur la propriété vieMin
	 */
	public IntegerProperty getVieMin() {
		return vieMin;
	}
	
	/**
	 * getter sur la propriété vieMax
	 */
	public IntegerProperty getVieMax() {
		return vieMax;
	}
	
	/**
	 * getter sur la propriété iter
	 */
	public IntegerProperty getIter() {
		return iter;
	}
	
	/**
	 * getter sur la propriété sleepTime
	 */
	public DoubleProperty getSleepTime() {
		return sleepTime;
	}
	
	
	
	/**
	 * getter sur la taille de la matrice
	 */
	public int getTaille() {
		return this.matrice.getTailleMatrice();
	}
	
	/**
	 * evolution de l'ensemble de la matrice
	 */
	public void evoluerMatrice() {
		if(this.etat.getValue()) {
		this.matrice.evoluerMatrice(this.mortSolitude.getValue(), this.mortAsphyxie.getValue(), this.vieMin.getValue(),
				this.vieMax.getValue());
		this.iter.setValue(this.iter.getValue()+1);
		}
	}

	/**
	 * getter sur la matrice tampon
	 */
	public MatriceTampon getTampon() {
		return tampon;
	}
	
	/**
	 * getter sur la taille de la matrice tampon
	 */
	public int getTailleTampon() {
		return this.tampon.getTailleMatrice();
	}

	/**
	 * setter sur la taille de la matrice tampon
	 * @param newSize : nouvelle taille
	 */
	public void modifierTailleTampon(int newSize) {
		if(newSize<ModelJeuDeLaVie.TAILLE_TAMPON_MAX)
		this.tampon.modifierTaille(newSize);
	}
	
	/**
	 * modifier la valeur d'une cellule dans la matrice tampon
	 * @param x : ligne de la cellule
	 * @param y : colone de la cellule
	 * @param valeur : nouvelle valeur de la cellule
	 * @throws Exception
	 */
	public void setTamponCelluleValeur(int x, int y, boolean valeur) throws Exception {
		
			this.tampon.setCelluleValeur(x, y, valeur);
	}
	
	/**
	 * inserer une matrice tampon dans la matrice du jeu de la vie, dans une position x,y
	 * @param x : ligne
	 * @param y : colone
	 */
	public void insererTampon(int x ,int y) {
		Matrice.copyMatriceInside(this.getMatriceTampon(), this.getMatriceJeu(), x,y);
	}
	
	
	/**
	 * initialiser la matrice tampon, par une copie outside de la matrice du jeu dans une position x,y,  de la matrice tampon
	 * @param x : postion ligne dans la matrice du jeu
	 * @param y : position colonne dans la matrice du jeu
	 */
	public void initTamponMatrice(int x,int y) {
		Matrice.copyMatriceOutside( this.getMatriceJeu(),this.getMatriceTampon(), x,y,this.tampon.getTailleMatrice());
		
	}
	
	/**
	 * initialiser la matrice tampon, par un modele
	 * @param m
	 */
	public void initTamponModel(Modeles m) {
		this.tampon.initModele(m);
		
	}
	
	
	/***
	 * getter sur le parametre VIE_MIN_LISTE
	 * @return vieMinListe
	 */
	public static int[] getVieMinListe() {
		return VIE_MIN_LISTE;
	}

	/**
	 * getter sur le parametre VIE_MAX_LISTE
	 * @return the vieMaxListe
	 */
	public static int[] getVieMaxListe() {
		return VIE_MAX_LISTE;
	}

	/**
	 * getter sur le parametre MORT_SOLITUDE_LISTE
	 */
	public static int[] getMortSolitudeListe() {
		return MORT_SOLITUDE_LISTE;
	}

	/**
	 * getter sur la propriété d'état
	 */
	public BooleanProperty getEtat() {
		return etat;
	}

	/**
	 * getter sur MORT_ASPHYXIE_LISTE
	 */
	public static int[] getMortAsphyxieListe() {
		return MORT_ASPHYXIE_LISTE;
	}

	/**
	 * getter sur la taille maximale de la matrice
	 */
	public static int getTailleMatriceMax() {
		return TAILLE_MATRICE_MAX;
	}
	
	/**
	 * inverse la valeur d'une cellule de la matrice tampon vers la valeur oposée
	 * @param x : position ligne de la cellule
	 * @param y : position colonne de la cellule
	 * @return false dans le cas où la cellule est invalide
	 * 			true dans le cas où la cellule est valide
	 */
	public boolean switchCelluleValeurTampon(int x, int y) {
			return this.tampon.switchCelluleValeur(x, y);
			
	}
	
	/**
	 * inverse la valeur d'une cellule de la matrice du jeu, vers la valeur oposée
	 * @param x position ligne de la cellule
	 * @param y position colonne de la cellule
	 * @return false dans le cas où la cellule est invalide
	 * 			true dans le cas où la cellule est valide
	 */
	public boolean switchCelluleValeurMatrice(int x, int y) {
		return this.matrice.switchCelluleValeur(x, y);

	}
	
	/**
	 * verifie ls parametres (vie Maximale, vie Minimale) et (mort Asphyxie, mort Solitude) 
	 */
	public boolean isValid() {
		return (this.getMortAsphyxie().getValue()>=this.getMortSolitude().getValue())&&
				(this.getVieMax().getValue()>=this.getVieMin().getValue());
	}
	
	/**
	 * retourn la liste des problèmes dans le cas où isValid return false
	 */
	public String getProblemes()
	{
		String pro="";
		if(this.getMortAsphyxie().getValue()<this.getMortSolitude().getValue()) 
		{
			pro="erreur : MortAsphyxie > MortSolitude \n";
		}
		if(this.getVieMax().getValue()<this.getVieMin().getValue()) 
		{
			pro=pro+"erreur : VieMin > VieMax \n";
		}
		return pro;
	}

	/**
	 * @return the mortSolitudeIndex
	 */
	public static int getMortSolitudeIndex() {
		return MORT_SOLITUDE_INDEX;
	}

	/**
	 * @return the mortAsphyxieIndex
	 */
	public static int getMortAsphyxieIndex() {
		return MORT_ASPHYXIE_INDEX;
	}

	/**
	 * @return the vieMinIndex
	 */
	public static int getVieMinIndex() {
		return VIE_MIN_INDEX;
	}

	/**
	 * @return the vieMaxIndex
	 */
	public static int getVieMaxIndex() {
		return VIE_MAX_INDEX;
	}
	
}
