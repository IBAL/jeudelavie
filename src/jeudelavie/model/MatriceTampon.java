package jeudelavie.model;

public class MatriceTampon extends Matrice {

	/**
	 * la taille de la matrice par defaut
	 */
	public static final int TAILLE_PAR_DEFAUT = 10;

	/**
	 * constructeur par defaut de la matrice represantent le tampon
	 */
	public MatriceTampon() {
		this(MatriceTampon.TAILLE_PAR_DEFAUT);

	}

	/**
	 * constructeur parametre de la matrice represantent le tampon
	 * @param taille : taille de matrice
	 */
	public MatriceTampon(int taille) {
		super(taille);
	}
	
	/**
	 * Initialise une matrice, par la matrice equivalent par la matrice équivalent au model 
	 * @param model
	 */
	public void initModele(Modeles model) {

		this.modifierTaille(Factory.getExempleTaille(model));
		try {
			this.initMatriceCopy(Factory.getExemple(model));
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}