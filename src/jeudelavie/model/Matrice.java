package jeudelavie.model;

import java.util.Random;



public class Matrice {

	/**
	 * la taille de la matrice
	 */
	private int tailleMatrice;

	

	/**
	 * matrice
	 */
	private boolean[][] matrice;

	/**
	 * copie de matrice
	 */
	private boolean[][] copieMatrice;

	/**
	 * construire une matrice de taille
	 * @param taille : taille de la matrice
	 */
	public Matrice(int taille) {
		this.tailleMatrice = taille;
		this.matrice = new boolean[this.tailleMatrice][this.tailleMatrice];
	}

	/**
	 * verifie pour une matrice donnée, si les deux indices sont valide dans la matrice
	 * @param x : l'indice x
	 * @param y : l'indice y
	 */
	public boolean celluleValide(int x, int y) {
		return (x < this.tailleMatrice || y < this.tailleMatrice);
	}

	/**
	 *  manipuler l'état d'une cellule précise
	 * @param x : indice x
	 * @param y : indice y
	 * @param valeur : true ou flase
	 * @throws Exception
	 */
	
	public void setCelluleValeur(int x, int y, boolean valeur) throws Exception {
		if (celluleValide(x, y)) {
			this.matrice[x][y] = valeur;
		} else {
			throw new Exception("hors index");
		}
	}

	/**
	 * récupérer dans un tableau de booléens une image du plateau
	 */
	public boolean[][] getMatrice() {
		return this.matrice;
	}

	/**
	 * initialiser le tableau : avec des cases vides
	 */
	public void initMatriceVide() {
		for (int i = 0; i < this.tailleMatrice; i++) {
			for (int j = 0; j < this.tailleMatrice; j++) {
				this.matrice[i][j] = false;
			}

		}
	}

	/**
	 * initialiser le tableau : avec une copie d'un tableau de booléens de la même dimension
	 * 
	 * @throws Exception
	 */
	public void initMatriceCopy(boolean[][] newMatrice) throws Exception {
		if (this.matrice.length != newMatrice.length)
			throw new Exception("les matrices n'ont pas la même longueur");
		for (int i = 0; i < this.tailleMatrice; i++) {
			if (this.matrice[i].length != newMatrice[i].length)
				throw new Exception("les matrices n'ont pas la même longueur");
		}
		copyMatrice(newMatrice, this.matrice);

	}

	/**
	 * initialiser le tableau : aléatoirement avec une certaine probabilité
	 * @param proba :probabilité( percentage % ) :probabilité qu'une case soit
	 *              occupée par une cellule vivante
	 */
	public void initMatriceAlea(int proba) {
		Random r = new Random();
		for (int i = 0; i < this.tailleMatrice; i++) {
			for (int j = 0; j < this.tailleMatrice; j++) {
				if (proba >= 100 || r.nextInt(100) < proba)
					this.matrice[i][j] = true;
				else
					this.matrice[i][j] = false;
			}

		}
	}

	/**
	 * modifier la taille de la matrice
	 * @param newSize: nouvelle taille de matrice
	 */
	public void modifierTaille(int newSize) {
		if (newSize > 0) {
			if (this.tailleMatrice < newSize)
				this.upTaille(newSize);
			if (this.tailleMatrice > newSize)
				this.downTaille(newSize);
		}
	}

	/**
	 * agrondir la taille de la matrice
	 * @param newSize : nouvelle taille de matrice
	 */
	public void upTaille(int newSize) {

		boolean[][] matriceCopy = new boolean[this.tailleMatrice][this.tailleMatrice];
		copyMatrice(this.matrice, matriceCopy);
		int N1 = this.tailleMatrice;
		int deff = (newSize - N1) / 2;
		this.tailleMatrice = newSize;
		this.matrice = new boolean[newSize][newSize];
		this.initMatriceVide();

		for (int i = 0; i < N1; i++) {
			for (int j = 0; j < N1; j++) {
				this.matrice[i + deff][j + deff] = matriceCopy[i][j];
			}
		}
		this.tailleMatrice = newSize;

	}

	/**
	 * getter sur taille matrice
	 * @return taille matrice
	 */
	public int getTailleMatrice() {
		return tailleMatrice;
	}
	
	/**
	 * getter sur la copie de la matrice
	 * @return  copieMatrice
	 */
	public boolean[][] getCopieMatrice() {
		return copieMatrice;
	}

	/**
	 * reduire la taille de la matrice
	 * @param nouvelle taille de matrice
	 */
	public void downTaille(int newSize) {
		boolean[][] matriceCopy = new boolean[newSize][newSize];
		for (int i = 0; i < newSize; i++) {
			for (int j = 0; j < newSize; j++) {
				matriceCopy[i][j] = this.matrice[i][j];
			}
		}
		this.tailleMatrice = newSize;
		this.matrice = matriceCopy;
		
	}

	/**
	 * verifie l'etat d'une cellule, soit dans la mtarice, ou la matrice copy
	 * @param x : position x de la cellule dans les lignes
	 * @param y : positon y de la cellule dans les colonnes
	 * @param copy : - true - on verifie dans la matrice copy
	 * 				 - false - on verifie dans la matrice
	 * @return etat : 1 vivante
	 * 				  0 morte
	 */
	public int isActive(int x, int y, boolean copy) {
		if (copy) {
			if (this.copieMatrice[x][y])
				return 1;
			return 0;
		} else {
			if (this.matrice[x][y])
				return 1;
			return 0;
		}
	}
	
	/**
	 * creation d'une copie matrice
	 */
	 protected void createCopieMatrice() 
	{
		this.copieMatrice = new boolean[this.getTailleMatrice()][this.getTailleMatrice()];
	}

	/**
	 * fait une copie d'une matrice source dans une matrice destination
	 * @param source : source de la copie
	 * @param distination : destination de la copie
	 */
	public static void copyMatrice(boolean[][] source, boolean[][] distination) {
		for (int i = 0; i < source.length; i++) {
			for (int j = 0; j < source[i].length; j++) {
				try {
					distination[i][j] = source[i][j];
				} catch (IndexOutOfBoundsException e) {
					e.printStackTrace();
				}
			}

		}
	}
	

	/**
	 * copie d'une matrice source dans matrice destination, dans une position x,y dans la destination
	 * @param source : source de la copie
	 * @param distination : destination de copie
	 * @param x : position dans lignes
	 * @param y : position dans les colonnes
	 */
	public static void copyMatriceInside(boolean[][] source, boolean[][] distination, int x, int y) {
		for (int i = 0; i < source.length; i++) {
			for (int j = 0; j < source[i].length; j++) {
				
				int xx = ((x + i) + distination.length) % distination.length;
				int yy = ((y + j) + distination.length) % distination.length;
				distination[xx][yy]=source[i][j];
			}

		}
	}
	
	/**
	 * fait une copie de taille size d'une partie dans une postion x,y d'une matrice source, dans une matrice destination,  
	 * @param source
	 * @param distination
	 * @param x : postion ligne
	 * @param y : position colonne
	 * @param size : taille de la copie
	 */
	public static void copyMatriceOutside(boolean[][] source, boolean[][] distination, int x, int y,int size) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				
				int xx = ((x + i) + source.length) % source.length;
				int yy = ((y + j) + source.length) % source.length;
				distination[i][j]=source[xx][yy];
			}

		}
	}
	
	/**
	 * Switch l'etat d'une cellule dans une position x,y à l'état opposé, dans la condition que la position valide
	 * @param x : position dans les lignes
	 * @param y : position dans les colonnes
	 * @return
	 */
	public boolean switchCelluleValeur(int x, int y)  {
		if (celluleValide(x, y)) {
			return this.matrice[x][y] =!this.matrice[x][y] ;
		} else {
			return false;
			
		}
	}


}
