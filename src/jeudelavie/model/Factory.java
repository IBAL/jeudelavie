package jeudelavie.model;

public interface Factory {
	
	/**
	 * matrice GLISSEUR2
	 */
	public static final boolean[][] GLISSEUR2= {
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,true,false,false,false},
			{false,false,false,false,false,false,false,true,false,false},
			{false,false,true,false,false,false,false,true,false,false},
			{false,false,false,true,true,true,true,true,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false}
	};
	
	/**
	 * matrice GLISSEUR3
	 */
	public static final boolean[][] GLISSEUR3= {
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,true,false,false,false},
			{false,false,false,false,false,false,false,true,false,false},
			{false,true,false,false,false,false,false,true,false,false},
			{false,false,true,true,true,true,true,true,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false}
	};
	
	/**
	 * matrice LIGNE3
	 */
	public static final boolean[][] LIGNE3= {
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,true,true,true,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false}
	};
	
	/**
	 * matrice LIGNE8
	 */
	public static final boolean[][] LIGNE8= {
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,true,true,true,true,true,true,true,true,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false},
			{false,false,false,false,false,false,false,false,false,false}
	};
	
	/**
	 * matrice MOULIN
	 */
	public static final boolean[][] MOULIN= {
			{true,true,true,true,true,true,false,true,true,false},
			{true,true,true,true,true,true,false,true,true,false},
			{false,false,false,false,false,false,false,true,true,false},
			{true,true,false,false,false,false,false,true,true,false},
			{true,true,false,false,false,false,false,true,true,false},
			{true,true,false,false,false,false,false,true,true,false},
			{true,true,false,false,false,false,false,true,true,false},
			{true,true,false,true,true,true,true,true,true,false},
			{true,true,false,true,true,true,true,true,true,false},
			{false,false,false,false,false,false,false,false,false,false}
	};
	
	/**
	 * 
	 * @param m le model motif
	 * @return matrice equivlente du model
	 */
	public static boolean[][] getExemple(Modeles m)
	{
		switch(m) 
		{
		case Glisseur2 :
			return GLISSEUR2;
		case Glisseur3:
			return GLISSEUR3;
		case Ligne3:
			return LIGNE3;
		case Ligne8 :
			return LIGNE8;
		case Moulin:
			return MOULIN;
		default :
			return null;
		}
	}
	
	/**
	 * 
	 * @param m model motif
	 * @return la taille matrice equivlente du model
	 */
	public static int getExempleTaille(Modeles m)
	{
		switch(m) 
		{
		case Glisseur2 :
			return GLISSEUR2.length;
		case Glisseur3:
			return GLISSEUR3.length;
		case Ligne3:
			return LIGNE3.length;
		case Ligne8 :
			return LIGNE8.length;
		case Moulin:
			return MOULIN.length;
		default :
			return 0;
		}
	}
	
	/**
	 * getter sur la liste de tout les models
	 * @return tableau de tout les models
	 */
	public static String[] getModelsArray()
	{
		return new String[]{"Glisseur2","Glisseur3","Ligne3","Ligne8","Moulin"};
	}
	
	public static Modeles getModel(String str)
	{
		return Modeles.valueOf(str);
	}
	
}



