package jeudelavie.model;





public class MatriceJeu extends Matrice{
	/**
	 * la taille de la matrice par defaut
	 */
	public static final int TAILLE_PAR_DEFAUT = 100;

	/**
	 * constructeur par defaut
	 */
	public MatriceJeu() {
		this(MatriceJeu.TAILLE_PAR_DEFAUT);

	}

	/**
	 * constructeur parametre
	 * @param taille : taille de matrice
	 */
	public MatriceJeu(int taille) {
		super(taille);
	}
	
	/**
	 * calcule de nombre de voisin active pour une cellule x,y
	 * @param x : position ligne
	 * @param y : position colonne
	 * @return nbr de voisin active
	 */
	public int calcnbVoisinesActive(int x, int y) {
		int nbVoisinesActives = 0;
		for (int i = -1; i <= 1; i++)
			for (int j = -1; j <= 1; j++) {
				if (i == 0 && j == 0)
					continue;
				int xx = ((x + j) + this.getTailleMatrice()) % this.getTailleMatrice();
				int yy = ((y + i) + this.getTailleMatrice()) % this.getTailleMatrice();
				nbVoisinesActives = nbVoisinesActives + isActive(xx, yy, true);
			}

		return nbVoisinesActives;
	}
	
	/**
	 * evolution d'une cellule par rapport aux voisines/ Met à jour les valeurs dans
	 * matrice à partir des valeurs dans matriceCopie.
	 * @param x numéro de ligne de la cellule qui doit evoluer
	 * @param y numéro de colonne de la cellule qui doit evoluer
	 */
	void evoluer(int x, int y, int mortSolitude, int mortAsphyxie, int vieMin, int vieMax) {
		int nbVoisinesActives = this.calcnbVoisinesActive(x, y);

		if (this.getCopieMatrice()[x][y]) {

			if (nbVoisinesActives > mortAsphyxie || nbVoisinesActives < mortSolitude) {

				this.getMatrice()[x][y] = false;
			}
		} else {

			if (nbVoisinesActives >= vieMin && nbVoisinesActives <= vieMax) {
				this.getMatrice()[x][y] = true;

			}
		}
	}

	/**
	 * evolution de l'ensemble des cellule d'une matrice
	 * @param mortSolitude
	 * @param mortAsphyxie
	 * @param vieMin
	 * @param vieMax
	 */
	public void evoluerMatrice(int mortSolitude, int mortAsphyxie, int vieMin, int vieMax) {
		
		this.createCopieMatrice();
		copyMatrice(this.getMatrice(), this.getCopieMatrice());
		for (int x = 0; x < this.getTailleMatrice(); x++)
			for (int y = 0; y < this.getTailleMatrice(); y++)
				evoluer(x, y, mortSolitude, mortAsphyxie, vieMin, vieMax);

	}

	
	
	

}
