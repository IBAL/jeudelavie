package jeudelavie.model;


//Ceci n'est qu'un test pour tester le modele, il ne fait pas partie de l'application
public class TestModele {

	public static void main(String[] args) {
		// base
		ModelJeuDeLaVie mjd = new ModelJeuDeLaVie(5);
		try {
			mjd.setCelluleValeur(0, 0, true);
			mjd.setCelluleValeur(0, 1, true);
			mjd.setCelluleValeur(0, 2, true);
			mjd.setCelluleValeur(1, 1, true);
			mjd.setCelluleValeur(1, 2, true);
			mjd.setCelluleValeur(1, 0, true);
			mjd.setCelluleValeur(2, 2, true);
			mjd.setCelluleValeur(2, 0, true);
			mjd.setCelluleValeur(2, 1, true);
			mjd.setCelluleValeur(2, 3, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		showMatrice(mjd.getMatriceJeu());

		System.out.println();
		System.out.println();
		System.out.println();

		mjd.modifierTaille(3);
		showMatrice(mjd.getMatriceJeu());

		System.out.println();
		System.out.println();
		System.out.println();

		mjd.modifierTaille(10);
		showMatrice(mjd.getMatriceJeu());
		System.out.println();
		System.out.println();
		System.out.println();

		boolean[][] copyTestM = new boolean[mjd.getTaille()][mjd.getTaille()];

		MatriceJeu.copyMatrice(mjd.getMatriceJeu(), copyTestM);
		mjd.initMatriceAlea(45);
		showMatrice(mjd.getMatriceJeu());

		System.out.println();
		System.out.println();
		System.out.println();
		try {
			mjd.initMatriceCopy(copyTestM);
		} catch (Exception e) {

			e.printStackTrace();
		}
		showMatrice(mjd.getMatriceJeu());

		// algo
		ModelJeuDeLaVie mjd2 = new ModelJeuDeLaVie(5);
		mjd2.initMatriceVide();
		mjd2.getMortAsphyxie().setValue(3);
		mjd2.getMortSolitude().setValue(2);

		try {

			mjd2.setCelluleValeur(2, 2, true);
			mjd2.setCelluleValeur(2, 1, true);
			mjd2.setCelluleValeur(2, 3, true);
		} catch (Exception e) {

			e.printStackTrace();
		}
		System.out.println();
		System.out.println();
		System.out.println();
		showMatrice(mjd2.getMatriceJeu());
		mjd2.evoluerMatrice();
		System.out.println();
		System.out.println();
		System.out.println();
		showMatrice(mjd2.getMatriceJeu());
		mjd2.evoluerMatrice();
		System.out.println();
		System.out.println();
		System.out.println();
		showMatrice(mjd2.getMatriceJeu());
		mjd2.evoluerMatrice();
		System.out.println();
		System.out.println();
		System.out.println();
		showMatrice(mjd2.getMatriceJeu());

		// tampon
		try {
			mjd2.setTamponCelluleValeur(1, 2, true);
			mjd2.setTamponCelluleValeur(0, 2, true);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		System.out.println();
		System.out.println();
		System.out.println();
		showMatrice(mjd2.getMatriceJeu());
		System.out.println();
		showMatrice(mjd2.getTampon().getMatrice());
		
		mjd2.insererTampon(2, 2);
		System.out.println();
		System.out.println();
		System.out.println();
		showMatrice(mjd2.getMatriceJeu());
		System.out.println();
		showMatrice(mjd2.getTampon().getMatrice());
		
		mjd2.initTamponModel(Modeles.Glisseur2);
		
		System.out.println();
		System.out.println();
		System.out.println();
		showMatrice(mjd2.getMatriceJeu());
		System.out.println();
		showMatrice(mjd2.getTampon().getMatrice());
		
		//test f
		mjd2.modifierTaille(mjd2.getTailleTampon());
		mjd2.insererTampon(0, 0);
		for(int i = 0 ;i<8;i++)
		{
			mjd2.evoluerMatrice();
			System.out.println();
			System.out.println();
			showMatrice(mjd2.getMatriceJeu());
		}

	}

	public static void showMatrice(boolean[][] matrice) {
		for (int i = 0; i < matrice.length; i++) {
			for (int j = 0; j < matrice[i].length; j++) {
				if (matrice[i][j])
					System.out.print("|o|");
				else
					System.out.print("| |");
			}

			System.out.println();
		}
	}

}
